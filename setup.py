from setuptools import setup, find_packages

setup(name='copymp3',
      version='1.0',
      author='Yurii Pysanko',
      author_email='yurayura1302@gmail.com',
      packages=find_packages(),
      entry_points={
          'console_scripts':
              ['copymp3 = scr.main:work']
      },

      package_data={
          '': ['*.json']
      }
      )
