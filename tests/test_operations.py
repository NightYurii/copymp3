import pytest
import os
from scr import operation_mp3
from tests import modul_test


def test_json():
    operation_mp3.json_unloading("D:/pytest/nice/", "D:/pytest/ni/")
    assert ("D:/pytest/nice/",
            "D:/pytest/ni/") == operation_mp3.json_loading()


def test_del_copy():
    test = modul_test.copy_sor_rec()
    assert operation_mp3.del_copy_file(
        "D:/pytest/nice/", "D:/pytest/ni/") == test


def test_del_dup():
    test = modul_test.copy_dupl()
    assert operation_mp3.chek_and_del_dplicate(
        "D:/pytest/nice/") == test


def test_moving():
    count = 0
    for file in os.listdir("D:/pytest/nice/"):
        if file.endswith(".mp3"):
            count += 1
    assert count == operation_mp3.moving(
        "D:/pytest/nice/", "D:/pytest/ni/")
