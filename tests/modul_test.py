import os
import shutil
from scr import operation_mp3


def copy_sor_rec():
    source, receiver = operation_mp3.json_loading()
    copy_files = 0
    for file in os.listdir(source):
        if file.endswith(".mp3"):
            copy_files += 1
            shutil.copy(os.path.join(source, file),
                        os.path.join(receiver, file))
        if copy_files == 3:
            break
    return copy_files


def copy_dupl():
    source, receiver = operation_mp3.json_loading()
    copy_files = 0
    for file in os.listdir(source):
        if file.endswith(".mp3"):
            copy_files += 1
            shutil.copy(os.path.join(source, file),
                        os.path.join(source, file[:-4]+'(1).mp3'))
        if copy_files == 3:
            break
    return copy_files
