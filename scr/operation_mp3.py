import os
import shutil
import filecmp
import json
from pathlib import Path

dir_path = os.path.dirname(os.path.realpath(__file__))
json_keys = "keys.json"


def del_copy_file(source, receiver):
    delete_file = list(set(os.listdir(source)) & set(os.listdir(receiver)))
    num_del_files = len(delete_file)
    for file in delete_file:
        os.remove(os.path.join(source, file))
    print("Deleted files: ", num_del_files)
    return num_del_files


def chek_and_del_dplicate(source):
    del_duplicate = 0
    for file in os.listdir(source):
        index_brack1 = file.rfind("(")
        index_brack2 = file.rfind(")")
        operlist = list(os.listdir(source))
        del operlist[operlist.index(file)]
        copy_file = file[:index_brack1 - len(file) - 1]
        if index_brack2 == len(file) - 5:
            for chek_file in operlist:
                copy_chek = chek_file[:-4]
                if copy_chek.count(copy_file):
                    if filecmp.cmp(source+chek_file,
                                   os.path.join(source, file)):
                        os.remove(os.path.join(source, file))
                        del_duplicate += 1
                        break
    print("Deleted duplicate files: ", del_duplicate)
    return del_duplicate


def copy(source, receiver):
    copy_files = 0
    for file in os.listdir(source):
        if file.endswith(".mp3"):
            copy_files += 1
            shutil.copyfile(os.path.join(source, file),
                            os.path.join(receiver, file))
    print("Moved files: ", copy_files)
    return copy_files


def first_fill_json(dir_path, json_keys):
    if os.stat(os.path.join(dir_path, json_keys)).st_size == 0:
        print("Input adress source(default: ", os.path.join(
            str(Path.home()), 'download'))
        source = input()
        print("Input adress receiver(default: ", os.path.join(
            str(Path.home()), 'music'))
        receiver = input()
        if not source:
            source = str(Path.home()) + '/download/'
            if not os.path.exists(source):
                os.makedirs(source)
            if receiver == '':
                receiver = str(Path.home()) + '/music/'
        if not receiver:
            receiver = str(Path.home()) + '/music/'
        json_unloading(source, receiver, dir_path, json_keys)


def json_unloading(source, receiver, dir_path, json_keys):
    with open(os.path.join(dir_path, json_keys), 'w') as f_obj:
        json.dump({'source': source, 'receiver': receiver},
                  f_obj, indent=4)


def json_loading(dir_path, json_keys):
    with open(os.path.join(dir_path, json_keys)) as json_data:
        data = json.load(json_data)
        source = data['source']
        receiver = data['receiver']
    return source, receiver


def chek_correct_address(source, receiver):
    while not os.path.exists(source):
        print("Source path does not exist, input true adress")
        source = input()
    if not os.path.exists(receiver):
        os.makedirs(receiver)
