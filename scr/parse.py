import argparse
import sys
from scr import operation_mp3


def createParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--path', nargs='+', type=str)
    parser.add_argument('-s', '--show', action='store_true', default=False)
    parser.add_argument('-c', '--change', action='store_true', default=False)
    return parser


def pars_command_line():
    parser = createParser()
    spaceparser = parser.parse_args()
    if spaceparser.path:
        operation_mp3.json_unloading(spaceparser.path[0], spaceparser.path[1])
    if spaceparser.change:
        print("Input adress source")
        source = input()
        print("Input adress receiver")
        receiver = input()
        operation_mp3.json_unloading(source, receiver)
    if spaceparser.show:
        source, receiver = operation_mp3.json_loading()
        print("source: ", source)
        print("receiver: ", receiver)
        sys.exit()
    return spaceparser
