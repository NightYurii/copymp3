import os
from scr import operation_mp3
from scr import parse


def work():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    json_keys = "keys.json"
    operation_mp3.first_fill_json(dir_path, json_keys)

    parse.pars_command_line()
    source, receiver = operation_mp3.json_loading(dir_path, json_keys)
    operation_mp3.chek_correct_address(source, receiver)
    operation_mp3.del_copy_file(source, receiver)
    operation_mp3.chek_and_del_dplicate(source)
    operation_mp3.copy(source, receiver)


if __name__ == "__main__":
    work()
